/* 
 * File:   comp_texteditor.cpp
 * Author: Pedro Luis
 * 
 * Created on 12 de septiembre de 2011, 1:19
 */

#include "comp_texteditor.h"

CompTextEditor::CompTextEditor(QMainWindow *parent) {
    initComponents(parent);
}

void CompTextEditor::initComponents(QMainWindow* parent) {
    centralWidget_ET = new QWidget(parent);
    //////////////////////////////////////
    initMenu_Bar_ET(parent);
    initTool_Bar_ET(parent);
    initStatus_Bar_ET(parent);
    ///////////////////////////////////////////////
    tabTextFile = new QTabWidget(parent);
    //////////////////////////////////////////////
    parent->setCentralWidget(tabTextFile);
}

void CompTextEditor::initMenu_Bar_ET(QMainWindow *parent) {
    menuBar_ET = new QMenuBar(parent);
    ///////////////////////////////////////////////////////////
    menus = new QMenu[4];
    menus[0].setTitle(QObject::tr("&File"));
    menus[1].setTitle(QObject::tr("&Edit"));
    menus[2].setTitle(QObject::tr("&View"));
    menus[3].setTitle(QObject::tr("&Help"));
    //////////////////////////////////////////////////////////
    itemsMenu.append(new QAction(QIcon(":/icons/New24.gif"),"New", parent));
    itemsMenu.append(new QAction(QIcon(":/icons/Open24.gif"),"Open",parent));
    itemsMenu.append(new QAction(QIcon(":/icons/Save24.gif"),"Save", parent));
    itemsMenu.append(new QAction(QIcon(":/icons/Close24.gif"),"Close", parent));
    itemsMenu.append(new QAction(QIcon(":/icons/Quit24.gif"),"Quit", parent));
    
    itemsMenu.at(0)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
    itemsMenu.at(1)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
    itemsMenu.at(2)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
    itemsMenu.at(3)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_W));
    itemsMenu.at(4)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q));
    //////////////////////////////////////////////////////////
    itemsMenu.append(new QAction(QIcon(":/icons/Copy24.gif"), "Copy", parent));
    itemsMenu.append(new QAction(QIcon(":/icons/Paste24.gif"), "Paste", parent));
    itemsMenu.append(new QAction(QIcon(":/icons/Cut24.gif"), "Cut", parent));
    itemsMenu.append(new QAction("Select All", parent));
    
    itemsMenu.at(5)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_C));
    itemsMenu.at(6)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_V));
    itemsMenu.at(7)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_X));
    itemsMenu.at(8)->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_A));
    //////////////////////////////////////////////////////////
    itemsMenu.append(new QAction("Tool Bar", parent));
    itemsMenu.append(new QAction("Status Bar", parent));
    
    itemsMenu.at(9)->setCheckable(true); itemsMenu.at(9)->setChecked(true);
    itemsMenu.at(10)->setCheckable(true); itemsMenu.at(10)->setChecked(true);
    //////////////////////////////////////////////////////////
    menus[0].addAction(itemsMenu.at(0)); 
    menus[0].addAction(itemsMenu.at(1)); 
    menus[0].addSeparator();
    menus[0].addAction(itemsMenu.at(2)); 
    menus[0].addAction(itemsMenu.at(3)); 
    menus[0].addSeparator();
    menus[0].addAction(itemsMenu.at(4)); 
    //////////////////////////////////////////////////////////
    menus[1].addAction(itemsMenu.at(5));
    menus[1].addAction(itemsMenu.at(6));
    menus[1].addAction(itemsMenu.at(7));
    menus[1].addSeparator();
    menus[1].addAction(itemsMenu.at(8));
    //////////////////////////////////////////////////////////
    menus[2].addAction(itemsMenu.at(9));
    menus[2].addAction(itemsMenu.at(10));
    //////////////////////////////////////////////////////////
    menuBar_ET->addSeparator();
    menuBar_ET->addMenu(&menus[0]);
    menuBar_ET->addMenu(&menus[1]);
    menuBar_ET->addMenu(&menus[2]);
    menuBar_ET->addMenu(&menus[3]);
    //////////////////////////////////////////////////////////
    parent->setMenuBar(menuBar_ET);
}

void CompTextEditor::initTool_Bar_ET(QMainWindow *parent) {
    toolBar_ET = new QToolBar(parent);
    toolBar_ET->setOrientation(Qt::Horizontal);
    toolBar_ET->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar_ET->setMovable(false);
    /////////////////////////////////////////////////////////////////////
    buttonsToolbar = new QPushButton[4];
    buttonsToolbar[0].setText("New");  
    buttonsToolbar[0].setToolTip("New File");
    buttonsToolbar[0].setIcon(QIcon(":/icons/New32.gif"));
    buttonsToolbar[1].setText("Open"); 
    buttonsToolbar[1].setToolTip("Open File");
    buttonsToolbar[1].setIcon(QIcon(":/icons/Open32.gif"));
    buttonsToolbar[2].setText("Save"); 
    buttonsToolbar[2].setToolTip("Save File");
    buttonsToolbar[2].setIcon(QIcon(":/icons/Save32.gif"));
    buttonsToolbar[3].setText("Close");
    buttonsToolbar[3].setToolTip("Close File");
    buttonsToolbar[3].setIcon(QIcon(":/icons/Close32.gif"));
    /////////////////////////////////////////////////////////////////////
    toolBar_ET->addSeparator();
    toolBar_ET->addWidget(&buttonsToolbar[0]); toolBar_ET->addSeparator();
    toolBar_ET->addWidget(&buttonsToolbar[1]); toolBar_ET->addSeparator();
    toolBar_ET->addWidget(&buttonsToolbar[2]); toolBar_ET->addSeparator();
    toolBar_ET->addWidget(&buttonsToolbar[3]); toolBar_ET->addSeparator();
    /////////////////////////////////////////////////////////////////////
    parent->addToolBar(Qt::LeftToolBarArea, toolBar_ET);
}

void CompTextEditor::initStatus_Bar_ET(QMainWindow *parent) {
   statusBar_ET = new QStatusBar(parent);
   /////////////////////////////////////////////////////////////////////
   zoom_AT.setOrientation(Qt::Horizontal);
   zoom_AT.setMinimum(10);
   zoom_AT.setMaximum(200);
   dir_AT.setEnabled(false);
   ////////////////////////////////////////////////////////////////////
   statusBar_ET->addWidget(new QLabel("Dir: "));
   statusBar_ET->addWidget(&dir_AT);
   statusBar_ET->addWidget(new QLabel("10"));
   statusBar_ET->addWidget(&zoom_AT);
   statusBar_ET->addWidget(new QLabel("200"));
   ////////////////////////////////////////////////////////////////////
   parent->setStatusBar(statusBar_ET);
}

CompTextEditor::~CompTextEditor() {
    delete centralWidget_ET;
    itemsMenu.clear();
    delete tabTextFile;
    delete menuBar_ET;
    delete toolBar_ET;
    delete statusBar_ET;
    delete []menus;
    delete []buttonsToolbar;
}

