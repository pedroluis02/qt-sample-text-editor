/* 
 * File:   com_texteditor.h
 * Author: Pedro Luis
 *
 * Created on 12 de septiembre de 2011, 1:19
 */

#ifndef COMP_TEXT_EDITOR_H
#define	COMP_TEXT_EDITOR_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QPushButton>
#include <QLineEdit>
#include <QSlider>
#include <QLabel>
#include <QAction>
#include <QMenu>
#include <QIcon>
#include <QHBoxLayout>
#include <QMessageBox>

#include "textarea_et.h"
#include "file_op.h"

class CompTextEditor {
public:
    CompTextEditor(QMainWindow *parent);
    virtual ~CompTextEditor();
private:
    void initComponents(QMainWindow *parent);
    void initMenu_Bar_ET(QMainWindow *parent);
    void initTool_Bar_ET(QMainWindow *parent);
    void initStatus_Bar_ET(QMainWindow *parent);
    ///////////////////////////////////////////
protected:
    QWidget *centralWidget_ET;
    QMenuBar *menuBar_ET;
    QToolBar *toolBar_ET;
    QStatusBar *statusBar_ET;
    QMenu *menus;
    QList <QAction*> itemsMenu; 
    QPushButton *buttonsToolbar;
    QLineEdit dir_AT;
    QSlider zoom_AT;
    
    ///////////////////////////
    QTabWidget *tabTextFile ;
    QList <TextAreaET*> areas;
    QStringList directorios;
};

#endif	/* COMP_TEXT_EDITOR_H */

