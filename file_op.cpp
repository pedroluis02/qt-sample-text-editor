/* 
 * File:   file_op.cpp
 * Author: Pedro Luis
 * 
 * Created on 13 de septiembre de 2011, 6:13
 */

#include "file_op.h"

FileOP::FileOP() {
}

QString FileOP::open(QString nameFile) {
    QFile in_AT(nameFile);
    if(!in_AT.open(QIODevice::ReadOnly | QIODevice::Text))
       return NULL;

    QString data_AT = ""; QTextStream in_F(&in_AT);
    data_AT += in_F.readLine(); 
    while(!in_F.atEnd())
        data_AT += "\n" + in_F.readLine();
    in_AT.close();

    return data_AT;
}

bool FileOP::save(QString fileName, QString data) {
    QFile ou_AT(fileName);
    if(!ou_AT.open(QIODevice::WriteOnly | QIODevice::Text))
       return false;
    QTextStream ou_F(&ou_AT);
        ou_F << data;
    ou_AT.close();
    return true;
}

int FileOP::existsDir(QStringList dirs, QString dir) {
    QStringList::iterator it; it = dirs.begin(); int i=0;
    while(it!=dirs.end()) {
        if(dir.compare(*it)==0)
           return i;
        i++; it++;
    }

    return -1;
}

FileOP::~FileOP() {
}

