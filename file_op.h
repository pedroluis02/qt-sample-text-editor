/* 
 * File:   fileop.h
 * Author: Pedro Luis
 *
 * Created on 13 de septiembre de 2011, 6:13
 */

#ifndef FILE_OP_H
#define	FILE_OP_H

#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QStringList>
#include <QList>
#include <QString>

class FileOP {
public:
    FileOP();
    static bool save(QString fileName, QString data);
    static QString open(QString fileName);
    static int existsDir(QStringList dirs, QString dir);
    virtual ~FileOP();
private:

};

#endif	/* FILE_OP_H */

