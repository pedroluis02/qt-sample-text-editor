/* 
 * File:   mainwindow.cpp
 * Author: Pedro Luis
 * 
 * Created on 12 de septiembre de 2011, 1:16
 */

#include "mainwindow.h"
#include "constants.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), 
        CompTextEditor(this) {
    setWindowTitle(APP_TITLE);
    resize(800, 600);
    setMinimumSize(600, 400);
    initComponents();
    eventMenuButtons();
    tabCreated = 0;
    newTextArea("No Saved", "default"+QString::number(tabCreated++)
            +"."+EXT_AT, "");
}

void MainWindow::initComponents() {
    setWindowIcon(QIcon());
    setVisible(true);
    ///////////////////////
}

void MainWindow::eventMenuButtons() {
    connect(tabTextFile, SIGNAL(currentChanged(int)), 
            this, SLOT(slotChangedTabAT(int)));
    //////////////////////////////////////////////////////////////////////
    connect(itemsMenu.at(0), SIGNAL(triggered()), this, SLOT(slotNew()));
    connect(itemsMenu.at(1), SIGNAL(triggered()), this, SLOT(slotOpen()));
    connect(itemsMenu.at(2), SIGNAL(triggered()), this, SLOT(slotSave()));
    connect(itemsMenu.at(3), SIGNAL(triggered()), this, SLOT(slotClose()));
    connect(itemsMenu.at(4), SIGNAL(triggered()), this, SLOT(slotQuit()));
    
    connect(itemsMenu.at(5), SIGNAL(triggered()), this, SLOT(slotCopy()));
    connect(itemsMenu.at(6), SIGNAL(triggered()), this, SLOT(slotPaste()));
    connect(itemsMenu.at(7), SIGNAL(triggered()), this, SLOT(slotCut()));
    connect(itemsMenu.at(8), SIGNAL(triggered()), this, SLOT(slotAll()));
    
    connect(itemsMenu.at(9), SIGNAL(triggered()), this, SLOT(slotViewTB()));
    connect(itemsMenu.at(10), SIGNAL(triggered()), this, SLOT(slotViewSB()));    
    //////////////////////////////////////////////////////////////////////
    connect(&buttonsToolbar[0], SIGNAL(clicked()), this, SLOT(slotNew()));
    connect(&buttonsToolbar[1], SIGNAL(clicked()), this, SLOT(slotOpen()));
    connect(&buttonsToolbar[2], SIGNAL(clicked()), this, SLOT(slotSave()));
    connect(&buttonsToolbar[3], SIGNAL(clicked()), this, SLOT(slotClose()));
    ///////////////////////////////////////////////////////////////////////
    connect(&zoom_AT, SIGNAL(valueChanged(int)), this, SLOT(slotZoom(int)));
}

void MainWindow::newTextArea(QString dir, QString name, QString data_text) {
    directorios.append(dir);
    areas.append(new TextAreaET(tabTextFile));
    areas.at(areas.size()-1)->setText(data_text);
    tabTextFile->addTab(areas.at(areas.size()-1), QIcon(":/icons/pl.gif"), name);
}

void MainWindow::removeTabAT(int index) {
    directorios.removeAt(index);
    areas.removeAt(index);
    tabTextFile->removeTab(index);
}

void MainWindow::closeTextArea(int index) {
    if(areas.at(index)->document()->isModified()) {
       QString tabTitle = tabTextFile->tabText(index);
       int bac = QMessageBox::question(this, "Close",
               "Do you want to save changes? \n" +
                          tabTitle,
                          "Yes", "No", "Cancel");
       if(bac==2)
          return; 

       if(bac==0) {
           if(directorios.at(index).compare("No Saved")==0) {
              save_AT(index);
              if(directorios.at(index).compare("No Saved")==0)
                 return;
           }
           else
               FileOP::save(directorios.at(index),
                       areas.at(index)->toPlainText());
       }
    }

    removeTabAT(index);
    if(tabTextFile->count()==0)
       exit(0);
}

void MainWindow::slotNew() {
    newTextArea("No Saved", "default"+QString::number(tabCreated++)
            + "." + EXT_AT, "");
}

void MainWindow::slotOpen() {
    open_AT();
    tabTextFile->setCurrentIndex(tabTextFile->count()-1);
}

void MainWindow::slotSave() {
    int index = tabTextFile->currentIndex();
    if(areas.at(index)->document()->isModified()) {
       if(directorios.at(index).compare("No Saved")==0)
          save_AT(index);
       else
           FileOP::save(directorios.at(index),
                   areas.at(index)->toPlainText());
    }
}

void MainWindow::slotClose() {
    int index = tabTextFile->currentIndex();
    closeTextArea(index);    
}

void MainWindow::slotQuit() {
    for(int i=tabTextFile->count()-1; i>=0; i--)
        closeTextArea(i);
    exit(0);
}

////////////////////////////////////////////////
void MainWindow::slotCopy() {
    int index = tabTextFile->currentIndex();
    if(areas.at(index)->textCursor().selectedText().length() > 0)
        areas.at(index)->copy();
}

void MainWindow::slotCut() {
    int index = tabTextFile->currentIndex();
    if(areas.at(index)->textCursor().selectedText().length() > 0)
        areas.at(index)->cut();
}

void MainWindow::slotPaste() {
    areas.at(tabTextFile->currentIndex())->paste();
}

void MainWindow::slotAll(){
    areas.at(tabTextFile->currentIndex())->selectAll();
}

////////////////////////////////////////////////
void MainWindow::slotViewTB() {
    toolBar_ET->setVisible(itemsMenu.at(9)->isChecked());
}

void MainWindow::slotViewSB() {
    statusBar_ET->setVisible(itemsMenu.at(10)->isChecked());
}

////////////////////////////////////////////////
void MainWindow::slotChangedTabAT(int index) {
    if(index>=0)
       dir_AT.setText(directorios.at(index));
}

void MainWindow::slotZoom(int value) {
    int index = tabTextFile->currentIndex();
    zoom_AT.setToolTip("value: "+QString::number(value));
    areas.at(index)->selectAll();
    areas.at(index)->setFontPointSize((qreal) value);
}

void MainWindow::open_AT() {
    QString nameFile = NULL;
    nameFile = QFileDialog::getOpenFileName(this, "Open File "+EXT_AT.toUpper(), 
                       "/home", 
                       "Text " + EXT_AT.toUpper()+" (*."+EXT_AT+")");
    if(nameFile==NULL)
       return;
    else if(FileOP::existsDir(directorios, nameFile)>=0)
       return;
    QString data_text = FileOP::open(nameFile);
    newTextArea(nameFile, QDir(nameFile).dirName(), data_text);
}

void MainWindow::save_AT(int index) {
    QString nameFile = NULL;
    nameFile = QFileDialog::getSaveFileName(this, "Save File "+EXT_AT.toUpper(), 
                       "/home", 
                       "Text " + EXT_AT.toUpper() + " (*."+EXT_AT+")");
    if(nameFile==NULL)
       return;
    FileOP::save(nameFile, areas.at(index)->toPlainText());
}

void MainWindow::closeEvent(QCloseEvent *) {
    for(int i=tabTextFile->count()-1; i>=0; i--)
        closeTextArea(i);
    exit(0);
}

MainWindow::~MainWindow() {
}

