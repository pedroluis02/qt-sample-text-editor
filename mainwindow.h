/* 
 * File:   mainwindow.h
 * Author: Pedro Luis
 *
 * Created on 12 de septiembre de 2011, 1:16
 */

#ifndef MAIN_WINDOW_H
#define	MAIN_WINDOW_H

#include "comp_texteditor.h"

#include <QCloseEvent>

class MainWindow : public QMainWindow, CompTextEditor {
    Q_OBJECT
public:
    MainWindow(QWidget *parent=0);
    void open_AT();
    void save_AT(int index);
    virtual ~MainWindow();
private:
    void initComponents();
    void newTextArea(QString dir, QString name, QString data_text);
    void closeTextArea(int index);
    void removeTabAT(int index);
    void eventMenuButtons();
    void closeEvent(QCloseEvent *);
    //////////////////////////////////////////////////////////////////
    int tabCreated;
private slots:
    void slotNew();
    void slotOpen();
    void slotSave();
    void slotClose();
    void slotQuit();
    /////////////////
    void slotCopy();
    void slotCut();
    void slotPaste();
    void slotAll();
    
    void slotViewTB();
    void slotViewSB();
    ////////////////
    void slotZoom(int value);
    void slotChangedTabAT(int index);
};

#endif	/* MAIN_WINDOW_H */

