/* 
 * File:   textarea_et.h
 * Author: Pedro Luis
 *
 * Created on 12 de septiembre de 2011, 1:42
 */

#ifndef TEXT_AREA_ET_H
#define	TEXT_AREA_ET_H

#include <QWidget>
#include <QTextEdit>
#include <QFont>

class TextAreaET : public QTextEdit {
public:
    TextAreaET(QWidget *parent=0);
    virtual ~TextAreaET();
private:
    void initComponents();
};

#endif	/* TEXT_AREA_ET_H */

