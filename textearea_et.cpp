/* 
 * File:   textare_et.cpp
 * Author: Pedro Luis
 * 
 * Created on 12 de septiembre de 2011, 1:42
 */

#include "textarea_et.h"

TextAreaET::TextAreaET(QWidget *parent) : QTextEdit(parent) {
    initComponents();
}

void TextAreaET::initComponents() {
    setFont(QFont("System", 10));
}

TextAreaET::~TextAreaET() {
}

